<?php
/* ==================
 * $Lists
 */

// let's create the function for the custom type
function post_type_lists() {
	// creating (registering) the custom type
	register_post_type( 'list', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
	 	// let's now add all the options for this post type
		array('labels' => array(
			'name' => __('Lists', 'bonestheme'), /* This is the Title of the Group */
			'singular_name' => __('List', 'bonestheme'), /* This is the individual type */
			'all_items' => __('All Lists', 'bonestheme'), /* the all items menu item */
			'add_new' => __('Add New List', 'bonestheme'), /* The add new menu item */
			'add_new_item' => __('Add New List ', 'bonestheme'), /* Add New Display Title */
			'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			'edit_item' => __('Edit List', 'bonestheme'), /* Edit Display Title */
			'new_item' => __('New List', 'bonestheme'), /* New Display Title */
			'view_item' => __('View Lists', 'bonestheme'), /* View Display Title */
			'search_items' => __('Search Lists', 'bonestheme'), /* Search Custom Type Title */
			'not_found' =>  __('Nothing found.', 'bonestheme'), /* This displays if there are no entries yet */
			'not_found_in_trash' => __('Nothing found in the trash', 'bonestheme'), /* This displays if there is nothing in the trash */
			'parent_item_colon' => ''
			), /* end of arrays */
			'description' => __( 'Publish pretty book lists exported from Sierra', 'bonestheme' ), /* Custom Type Description */
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 4, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			'rewrite'	=> array( 'slug' => 'list', 'with_front' => false ), /* you can specify it's url slug */
			'has_archive' => 'lists', /* you can rename the slug here */
			'capability_type' => 'post',
			'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			'supports' => array( 'title', 'editor', 'author', 'thumbnail' )
	 	) /* end of options */
	); /* end of register post type */

	/* this ads your post categories to your custom post type */
	//register_taxonomy_for_object_type('category', 'spotlight_databases');
	/* this ads your post tags to your custom post type */
	//register_taxonomy_for_object_type('post_tag', 'list');

}

	// adding the function to the Wordpress init
	add_action( 'init', 'post_type_lists');

?>
