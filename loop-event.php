<?php
// Variables
$count 		= 0;
$loop 		= ( is_tax() ? 'archive' : 'default' );
$taxonomy 	= ( is_tax() ? ( is_tax('series') ? 'series' : 'event_type' ) : null );
$filter 	= get_query_var( 'for' );
$keyword	= get_query_var( 'search' );
$audience 	= ( $filter === 'public' || 'academic' ? $filter : '' );
$paged = ( get_query_var( 'paged') ? get_query_var( 'paged' ) : 1 );

// Build the WP_Query
$the_query = asl_query_events( $loop, $audience, $keyword, $taxonomy );

// Search Phrase
echo ( $keyword ? '<p>Here are all the ' . ( $filter ? '<strong>' . $filter . '</strong>' : '') . ' events we could find matching <strong>' . $keyword . '</strong>:</p>': '' );

// The Loop
if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();

/* ==================
 *  Layout Options
 */ $title 					= get_the_title();

/* ==================
 * Date Options
 */	$allday					= false;
	$multiday				= false;
	$date_start				= get_post_meta( get_the_ID(), 'event_start', true );
	$date_options			= get_post_meta( get_the_ID(), 'scheduling_options', true );
	$start 					= $date_start;
	$end 					= $date_start;

	/**
	 * If there the scheduling_options array is empty, then
	 * we will set some defaults.
	 */
	if ( !$date_options ) {
		$event_start_time = get_post_meta( get_the_ID(), 'event_start_time', true );
		$event_end_time = get_post_meta( get_the_ID(), 'event_end_time', true );
		$start = $date_start . $event_start_time;
		$end 	= $date_start . $event_end_time;

	} else {

		/**
		 * If the event is checked "All Day", set the variable to true.
		 * Otherwise, fetch the end time.
		 */

		if ( in_array( "allday", $date_options ) ) :

			$allday = true;

		else :

			$end = get_post_meta( get_the_ID(), 'event_end', true ) . get_post_meta( get_the_ID(), 'event_end_time', true );

		endif;

		/**
		 * If the event is checked "Multi-Day", set the variable to true
		 * and adjust $start to reflect just the date.
		 */

		if ( in_array( "multiday", $date_options ) ) :

			$multiday = true;
			$start = $date_start;
			$end = get_post_meta( get_the_ID(), 'event_end', true );

		endif;

	}
?>

<?php $count++; ?>
<div class="col-md--twelvecol">

	<article id="post-<?php the_ID(); ?>" <?php post_class('card clearfix'); ?> itemscope itemtype="http://schema.org/Event" role="article">

		<span class="card__color-code <?php echo ( has_term( 'teens', 'library-audience' ) ? 'card__color-code--teens' : ( has_term( 'kids', 'library-audience' ) ? 'card__color-code--kids' : '' ) )?>"></span>

		<header class="card__header" style="margin-bottom: .5em;">
			<a class="link--undecorated _link-blue" href="<?php the_permalink() ?>" itemprop="url">
				<h2 class="menu__item__title" style="margin-bottom: .25em;"><?php the_title(); ?></h2>
			</a>
			<p class="no-margin small-text">

				<time class="time" datetime="<?php echo $start . ( $multiday === true ? '-' . $end : ''); ?>">
					<span itemprop="startDate" content="<?php echo $start ?>">
						<b><?php echo date('F jS', strtotime($start) ); echo ( date( 'Y' ) == date( 'Y', strtotime( $start ) ) ? '' : date( ', Y ', strtotime( $start ) ) ); ?></b>
					</span>
					<?php echo ( $multiday === false ? '' : '&mdash; <span itemprop="endDate" content="' . $end . '"><b>' . date('F jS', strtotime($end) ) . '</b></span>' ); ?>

					<?php if ( !$allday ) : ?>
					<span class="time__hours" style="color: #999;">
						<?php echo date( 'g:i a', strtotime( $start ) ); ?> - <?php echo date('g:i a', strtotime( $end )); ?>
					</span>
					<?php endif; ?>

				</time>
				<?php echo get_the_term_list( $post->ID, 'series', '| <span itemprop="superEvent">', ', ', '</span>'); ?>
			</p>

		</header>

		<section class="content">

			<p class="no-margin"><?php echo ( has_excerpt() ? get_the_excerpt() : '' ); ?></p>
		</section>

	</article>
</div>
<?php //endif; ?>
<?php endwhile; ?>
<?php wp_reset_postdata(); ?>
<?php endif; ?>

<?php if ( $count === 0 ) : ?>
	<p>Sorry. We couldn't find anything.</p>
<?php endif; ?>

<nav class="align-center pagination">
	<?php previous_posts_link( 'Previous' ); ?><?php ( $count === 0 || $count < 20 ? '' : ( $paged < 2  ? next_posts_link( 'More Events') : next_posts_link( 'Even More Events' ) ) ); ?>
</nav>
