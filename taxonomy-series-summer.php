<?php get_header(); ?>

<style>

	.card__media--grayscale {

		transition: all .2s ease-out;
	}

	.card__media--grayscale img {
		-webkit-filter: grayscale(100%);
		filter: grayscale(100%);
		-webkit-transition: all .2s ease-out;
		-moz-transition: all .2s ease-out;
		transition: all .2s ease-out;
	}
	.card__media--grayscale:hover img,
	.card__media--grayscale:focus img {
		-webkit-filter: grayscale( 0% );
		filter: grayscale( 0% );
	}

	.section__title {

	}
</style>

<header>

<?php if ( get_query_var( 'for' ) === 'adults' ) : ?>
	<div class="clearfix wrap">

		<h2 class="hide-accessible">Earns prizes for reading!</h2>

		<div class="col-sm--twelvecol col-lg--sixcol align-center">

			<img data-src="http://sherman.library.nova.edu/sites/spotlight/files/2016/05/big-win<?php echo ( get_query_var( 'for' ) === 'adults' ? '-adult' : '' ); ?>.png" alt="The Big Win: Broward County Summer Reading" style="width: 100%; max-width:443px;">

			<div class="col-sm--tencol col-md--sevencol col-lg--eightcol col--centered align-right">
				<?php if ( get_query_var( 'for' ) === 'adults' ) : ?>
					<a href="http://sherman.library.nova.edu/sites/spotlight/files/2016/06/adult-summer-review-form.pdf" class="button button--default button--flat">Book Review Form <small>(PDF)</small></a>
				<?php endif;?>
				<a href="http://systems.library.nova.edu/form/view.php?id=88" class="button green button--flat">Sign Up</a>
			</div>

		</div><!--/.sixcol-->

		<div class="col-lg--fivecol hero">

			<?php if ( get_query_var( 'for' ) === 'adults' ) : ?>
				<p>	Summer is the time when libraries traditionally encourage children in their leisure
					reading with fun programming and prizes. Here, we invite adults to rediscover the
					joy of summer reading.
				</p>

				<p class="zeta">Sign up and participate. Download or pick up a Summer Reading
					<a href="http://sherman.library.nova.edu/sites/spotlight/files/2016/05/adult-summer-review-form.pdf">Book Review Form</a>.
					Read or listen to any adult book between June 4th - August 6th. Take your completed Book Review Form
					to the library and drop it off into the contest box for a chance to win a weekly prize.


				</p>


			<?php else : ?>

			<p>	Children who complete
			the Reading Rewards program will have the option of selecting one book as a special prize, courtesy
			of the Erwin and Barbara Mautner Charitable Foundation's "Read for the Fun of It" Initiative.
			</p>

			<p class="zeta">Register online between
			May 20 - June 9, or in person on June 12. Each week (June 12 - August 6), children can
			visit the library, show their reading logs, pick a goodie from the
			treasure chest, and be entered to win big prizes.</p>

			<?php endif; ?>

		</div><!--/.sixcol-->

	</div>
	<?php //single_cat_title(); ?>
<?php endif; ?>
</header>

<main id="content" ng-app="publicServices">

	<div class="has-cards hero">

		<nav class="wrap clearfix" role="navigation" id="upcoming">

			<div class="col-md--fourcol">
				<div class="card <?php echo ( get_query_var( 'for' ) === 'kids' ? 'card--shadow' : '' ); ?>">
					<div class="card__media <?php echo ( get_query_var('for') != 'kids' ? 'card__media--grayscale' : '' ); ?>">
						<a href="http://public.library.nova.edu/summer/?for=kids#upcoming">
							<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/04/kid-reading.jpg">
						</a>
					</div>
					<div class="card__header clearfix" style="position: relative;">
						<a class="align-center link link--undecorated _link-blue" href="http://public.library.nova.edu/summer/?for=kids">
						<span class="card__header__color-code card__color-code--kids"></span>
							<h2 class="menu__item__title no-margin">Children</h2>
						</a>
					</div>
				</div>
			</div>

			<div class="col-md--fourcol">
				<div class="card <?php echo ( get_query_var('for') ==='teens' ? '.card--shadow' : ''); ?>">
					<div class="card__media <?php echo ( get_query_var('for') != 'teens' ? 'card__media--grayscale': '' ); ?>">
						<a href="http://public.library.nova.edu/summer/?for=teens#upcoming">
							<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/04/teen-reading.jpg">
						</a>
					</div>
					<div class="card__header clearfix" style="position: relative;">
						<a href="http://public.library.nova.edu/summer/?for=teens" class="align-center link link--undecorated _link-blue">
							<span class="card__header__color-code card__color-code--teens"></span>
							<h2 class="menu__item__title no-margin">Teens</h2>
						</a>
					</div>
				</div>
			</div>

			<div class="col-md--fourcol">
				<div class="card <?php echo ( get_query_var('for') ==='adults' ? '.card--shadow' : '' ); ?>">
					<div class="card__media <?php echo ( get_query_var('for') != 'adults' ? 'card__media--grayscale' : '' );?>">
						<a href="http://public.library.nova.edu/summer/?for=adults">
							<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/04/adult-reading.jpg">
						</a>
					</div>
					<div class="card__header clearfix" style="position: relative;">
						<a href="http://public.library.nova.edu/summer/?for=adults" class="align-center link link--undecorated _link-blue">
						<span class="card__header__color-code"></span>
							<h2 class="menu__item__title no-margin">Adults</h2>
						</a>
					</div>
				</div>
			</div>

		</nav>

		<div class="wrap clearfix">

				<aside class="col-md--sixcol col-lg--fourcol">

					<?php if ( get_query_var( 'for' ) === 'kids' ) : ?>
					<div class="card">
						<div class="card__media">
							<a href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=kids">
								<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/04/I-Aint-Gonna-Paint-No-More-e1462195646595.jpg" alt="An illustration of a girl painting her face">
							</a>
						</div>
						<div class="card__header clearfix">
							<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=kids">
								<h2 class="menu__item__title no-margin">Our Favorite Children's Books</h2>
							</a>
						</div>
					</div>
					<?php elseif ( get_query_var( 'for' ) === 'teens' ) : ?>
						<div class="card">
							<div class="card__media">
								<a href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=teens">
									<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/03/warm_bodies-wide-e1462197968197.jpg" alt="Warm Bodies wallpaper">
								</a>
							</div>
							<div class="card__header clearfix">
								<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/?for=teens">
									<h2 class="menu__item__title no-margin">YA Movie and Book Lists</h2>
								</a>
							</div>
						</div>

					<?php else : ?>
						<div class="card">
							<div class="card__media">
								<a href="http://sherman.library.nova.edu/sites/spotlight/lists/">
									<img src="http://sherman.library.nova.edu/sites/spotlight/files/2016/03/Across-The-Universe-Desktop-Wallpapers-across-the-universe-trilogy-30111464-1920-1200-e1462198543645.jpg" alt="Movie poster for Across the Universe">
								</a>
							</div>
							<div class="card__header clearfix">
								<a class="link link--undecorated _link-blue" href="http://sherman.library.nova.edu/sites/spotlight/lists/">
									<h2 class="menu__item__title no-margin">We Make Movie and Book Lists</h2>
								</a>
							</div>
						</div>
					<?php endif; ?>

					<div class="card">
						<div class="card__media">
							<a href="http://public.library.nova.edu/card/?utm_source=summer-website&utm_medium=card&utm_campaign=Summer%20Card%20Drive">
								<img src="http://public.library.nova.edu/wp-content/uploads/2013/10/library-card-e1461006529894.jpg" alt="Alvin Sherman Library card">
							</a>
						</div>
						<div class="card__header clearfix">
								<h2 class="menu__item__title no-margin" style="float: left; position: relative; top: 3px;"><span style="color:#e2624f;">♥</span> Your Library</h2>
								<a href="http://public.library.nova.edu/card/?utm_source=summer-website&utm_medium=card&utm_campaign=Summer%20Card%20Drive" class="button button--flat button--small green no-margin small-text" style="float: right;">Get a Card</a>
						</div>
					</div>

				</aside>

		    <section class="col-md--sixcol col-lg--eightcol clearfix">

		    	<?php get_template_part( 'loop', 'event-card' ); ?>

				</section> <!-- end #main -->
			</div>
	</div>
</main> <!-- end #content -->

<?php get_footer(); ?>
